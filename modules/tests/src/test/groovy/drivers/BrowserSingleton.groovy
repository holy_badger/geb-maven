package drivers

import org.openqa.selenium.remote.RemoteWebDriver

@Singleton
class BrowserSingleton {
    RemoteWebDriver driver
}
