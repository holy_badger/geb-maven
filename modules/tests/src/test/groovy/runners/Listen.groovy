package runners

import drivers.BrowserSingleton
import org.junit.runner.Description
import org.junit.runner.notification.Failure
import org.junit.runner.notification.RunListener
import org.openqa.selenium.Cookie

class Listen extends RunListener {

    private boolean result = true

    void testFailure(Failure failure) {
        result = false
    }

    void testFinished(Description description) {
        Cookie cookie = new Cookie("zaleniumTestPassed", result.toString())
       BrowserSingleton.instance.driver.manage().addCookie(cookie)
        result = true
        Cookie cookie1 = new Cookie("zaleniumVideo", "false")
        BrowserSingleton.instance.driver.manage().addCookie(cookie1)
    }


}
