package runners

import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError
import runners.Listen;

class CustomRunner extends BlockJUnit4ClassRunner {

    private Listen seleniumRunListener;

    CustomRunner(Class klass) throws InitializationError {
        super(klass);
        seleniumRunListener = new Listen();
    }

     void run(final RunNotifier notifier) {
        notifier.addListener(seleniumRunListener);
        super.run(notifier);
    }
}
