package pages

import geb.Page

class GebishOrgHomePage extends Page {

   private final String t='avc'

    static at = { title == "Geb - Very Groovy Browser Automation" }

    static content = {
        manualsMenu { module(ManualsMenuModule) }
        mainMenu {module(MainMenuModule)}
    }

}
