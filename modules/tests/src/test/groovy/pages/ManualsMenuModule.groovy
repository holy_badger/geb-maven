package pages

import geb.Module
import org.openqa.selenium.Cookie

class ManualsMenuModule extends Module {
    static content = {
        toggle { $("div.menu a.manuals") }
        linksContainer { $("#manuals-menu") }
        links { linksContainer.find("a") }
    }

    void open() {
        Cookie cookie = new Cookie("zaleniumMessage", "Open manuals")
        browser.driver.manage().addCookie(cookie);
        toggle.click()
        waitFor { !linksContainer.hasClass("animating") }
    }

   void clickItem(int index) {
      Cookie cookie = new Cookie("zaleniumMessage", "Click manual for index " + index)
      browser.driver.manage().addCookie(cookie);
      links[index].click()

   }
}
