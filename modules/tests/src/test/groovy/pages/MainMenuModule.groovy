package pages

import badger.annotation.AllureStep
import geb.Module
import io.qameta.allure.Step

@AllureStep
class MainMenuModule extends Module {

    static content = {
        mainMenuBase {$( "div.menu") }
        items {mainMenuBase.$("a")}
        item { Item item -> mainMenuBase.$( "a", text: item.uiName) }
        expandedItemContainer { Item item -> $("div", id: "$item.cssName-menu")  }
    }


   void clickItem(Item itemName) {
        item(itemName).click()
        waitFor (10) {!expandedItemContainer(itemName).hasClass("animating") }
    }


    void verifyMenuItems(Item ... expected){
      assert  items.allElements()*.text.containsAll(expected*.uiName)
    }

    enum Item {
        API("API", "apis"), MANUAL ("Manual", "manuals"),
        MAIlING_LIST("Mailing Lists", "mailing-lists")

        Item(String uiName, String cssName){
            this.uiName=uiName
            this.cssName=cssName
        }

        String uiName
        String cssName
    }
}
