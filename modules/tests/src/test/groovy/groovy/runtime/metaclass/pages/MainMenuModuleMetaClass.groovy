package groovy.runtime.metaclass.pages

class MainMenuModuleMetaClass extends DelegatingMetaClass{
    MainMenuModuleMetaClass(MetaClass metaClass) { super(metaClass) }
    MainMenuModuleMetaClass(Class theClass) { super(theClass) }

    Object invokeMethod(Object object, String name, Object[] args) {
        println "META"
        super.invokeMethod(object,name,args)
    }
}
