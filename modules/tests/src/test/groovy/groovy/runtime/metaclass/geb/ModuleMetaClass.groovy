package groovy.runtime.metaclass.geb

class ModuleMetaClass extends DelegatingMetaClass{
    ModuleMetaClass(MetaClass metaClass) { super(metaClass) }
    ModuleMetaClass(Class theClass) { super(theClass) }

    Object invokeMethod(Object object, String name, Object[] args) {
        println "META"
        super.invokeMethod(object,name,args)
    }
}
