package tests

import drivers.BrowserSingleton
import geb.junit4.GebReportingTest
import io.qameta.allure.Lead
import io.qameta.allure.TmsLink
import meta.MyClass
import meta.OtherClass
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestName
import org.junit.runner.RunWith
import org.openqa.selenium.Cookie
import pages.GebishOrgHomePage
import pages.MainMenuModule
import pages.TheBookOfGebPage
import runners.CustomRunner

@RunWith(CustomRunner)
class GebishOrgTest extends GebReportingTest {

   @Rule
   public TestName name = new TestName()


   @Before
   void addCookie() {
      browser.to GebishOrgHomePage
      Cookie cookie = new Cookie("zaleniumTestName", name.methodName)
      BrowserSingleton.instance.driver.manage().addCookie(cookie)
      Cookie cookie1 = new Cookie("zaleniumVideo", "true")
      BrowserSingleton.instance.driver.manage().addCookie(cookie1)
   }

   @Test
   void test() {
      GebishOrgHomePage homePage = browser.at GebishOrgHomePage
      homePage.mainMenu.verifyMenuItems(MainMenuModule.Item.values())
      homePage.mainMenu.clickItem(MainMenuModule.Item.API)
   }


   @Test
   void 'can Get to TheCurrentBookOfGeb'() {
      GebishOrgHomePage homePage = browser.at GebishOrgHomePage
      homePage.manualsMenu.open()

      //first link is for the current manual
      assert homePage.manualsMenu.links[0].text().startsWith("current")

      homePage.manualsMenu.clickItem(0)
     TheBookOfGebPage bookOfGebPage= browser.at TheBookOfGebPage
   }

   @Test
   void canGetToTheCurrentBookOfGeb1() {
      manualsMenu.open()

      //first link is for the current manual
      assert manualsMenu.links[0].text().startsWith("urrent")

      manualsMenu.links[0].click()

      at TheBookOfGebPage
   }

   @Test
   void testMeta() {
      def obj = new MyClass()
      obj.saySmth("Hello")
   }

   @Test
   void testMeta1() {
      def obj = new OtherClass()
      obj.shoutOut("Hello")
   }

}
