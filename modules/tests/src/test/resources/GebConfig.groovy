/*
	This is the Geb configuration file.

	See: http://www.gebish.org/manual/current/#configuration
*/


import drivers.CustomDriver

import java.text.NumberFormat

Properties properties = new Properties()
File propertiesFile = new File('./src/main/resources/geb.properties')
propertiesFile.withInputStream {
    properties.load(it)
}

waiting {
    timeout = NumberFormat.getInstance().parse(properties.'waiting.timeout')
}

environments {

    // run via “./gradlew chromeTest”
    // See: http://code.google.com/p/selenium/wiki/ChromeDriver
    chrome {
        driver = CustomDriver.CHROME.driverClosure()
    }

    // run via “./gradlew chromeHeadlessTest”
    // See: http://code.google.com/p/selenium/wiki/ChromeDriver
    chromeHeadless {
        driver = CustomDriver.CHROME_HEADLESS.driverClosure()
    }

    // run via “./gradlew firefoxTest”
    // See: http://code.google.com/p/selenium/wiki/FirefoxDriver
    firefox {
        atCheckWaiting = 1
        driver = CustomDriver.FIREFOX.driverClosure()

    }
    remoteChrome {
        driver = CustomDriver.REMOTE_CHROME.driverClosure()
    }
   remoteFirefox {
      driver = CustomDriver.REMOTE_FIREFOX.driverClosure()
   }

    baseUrl = properties.'baseUrl'

}
