package badger.transformation

import io.qameta.allure.Step
import org.apache.commons.lang3.StringUtils
import org.codehaus.groovy.ast.ASTNode
import org.codehaus.groovy.ast.AnnotationNode
import org.codehaus.groovy.ast.ClassNode
import org.codehaus.groovy.ast.MethodNode
import org.codehaus.groovy.ast.expr.ConstantExpression
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.ASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation

@GroovyASTTransformation(phase = CompilePhase.SEMANTIC_ANALYSIS)
class AllureStepTransformation implements ASTTransformation {

   @Override
   void visit(ASTNode[] astNodes, SourceUnit sourceUnit) {
      def clazz = astNodes[1] as ClassNode
      def methods = clazz.methods
      methods.each {
         if (it.public) {
            AnnotationNode annot = new AnnotationNode(new ClassNode(Step))
            annot.addMember('value', new ConstantExpression(stepName(it)))
            it.addAnnotation(annot)
         }
      }
   }

   private String stepName(MethodNode methodNode) {
      def params = methodNode.getParameters()
      if (params) {
         params = params.collect { p -> "$p.name : {$p.name}" }
      }
     return StringUtils.splitByCharacterTypeCamelCase(methodNode.getName()).collect { it.capitalize() }.join(' ') + (params ? " with parameters : " + params : "")
   }
}
